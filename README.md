# donuts
This is a donut chart to experiment with the Octane features dropping in Ember JS (3.14)

You can try the
 [live donut hosted at Glitch!](https://speckle-technician.glitch.me/)

See the article that inspired this:
 [CSS-Tricks](https://css-tricks.com/building-a-donut-chart-with-vue-and-svg/)






## Credits

[CSS-Tricks](https://css-tricks.com/building-a-donut-chart-with-vue-and-svg/)



