'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'sparklines-donut',
    environment
  };

  return ENV;
};
