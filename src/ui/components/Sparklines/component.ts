import Component, {tracked} from '@glimmer/component';

export default class Sparklines extends Component {
  @tracked private subA = 10;
  @tracked private subB = 10;
  @tracked private subC = 10;
  @tracked private subD = 10;
  @tracked private subE = 10;
  @tracked
  get subs() {
    return [
      {count: this.subA},
      {count: this.subB},
      {count: this.subC},
      {count: this.subD},
      {count: this.subE}
    ];
  }

  private segmentAChange(e) {
    let s = this.parseSub(e);
    this.subA = s;
  }
  private segmentBChange(e) {
    let s = this.parseSub(e);
    this.subB = s;
  }
  private segmentCChange(e) {
    let s = this.parseSub(e);
    this.subC = s;
  }
  private segmentDChange(e) {
    let s = this.parseSub(e);
    this.subD = s;
  }
  private segmentEChange(e) {
    let s = this.parseSub(e);
    this.subE = s;
  }

  private parseSub(evt) {
    let v = parseInt(evt.target.value, 10);
    if (isNaN(v)) { return 0; }
    return v;
  }
}
