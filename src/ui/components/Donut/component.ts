import Component, {tracked} from '@glimmer/component';

export default class Donut extends Component {
  @tracked
  get fractions() {
    return this.args.segments.map((a) => a.count / this.dataTotal);
  }

  @tracked
  get dataTotal() {
    const reducer = (acc, cur) => acc + parseInt(cur.count, 10);
    return this.args.segments.reduce(reducer, 0);
  }

}
