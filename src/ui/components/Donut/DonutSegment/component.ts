import Component, {tracked} from '@glimmer/component';

export default class DonutSegment extends Component {
  @tracked private cx = 80;
  @tracked private cy = 80;
  @tracked private radius = 60;
  @tracked private colors = ['#96CCFF', '#FFDFDF', '#FBF1A9', '#9EEBCF', '#A463F2', '#FFA3D7'];

  @tracked
  get segFraction() {
    return this.args.fractions[this.args.index];
  }

  @tracked
  get circumference() {
    return 2 * Math.PI * this.radius;
  }

  @tracked
  get dashOffset() {
    let strokeDiff = this.segFraction * this.circumference;
    return this.circumference - strokeDiff;
  }

  @tracked
  get angleOffset() {
    // When index is zero, make the offset -90.
    // otherwise, add up the fractions before the current index.
    const reducer = (acc, cur, idx, src) => (idx < this.args.index) ? acc + cur : acc;
    let frac = this.args.fractions.reduce(reducer, 0);
    return -90 + frac * 360;
  }

  @tracked
  get color() {
    return this.colors[this.args.index];
  }

  @tracked
  get percentageLabel() {
    return `${Math.round(this.segFraction * 100)}%`;
  }

  @tracked
  get labelX() {
    let angle = this.segFraction * 360 / 2 + this.angleOffset;
    let radians = angle * (Math.PI / 180);
    return this.radius * Math.cos(radians) + this.cx;
  }
  @tracked
  get labelY() {
    let angle = this.segFraction * 360 / 2 + this.angleOffset;
    let radians = angle * (Math.PI / 180);
    return this.radius * Math.sin(radians) + this.cy;
  }

}
